#!/bin/bash
#Author:Jewl B. Johnson 
#Descr: Even or Odd

echo "Enter 10 numbers please"

i=1

while [ $i -le 10 ] 
do 

read X
if [ $(($X%2)) == 0 ]
then
	echo $X >> even.txt
else
	echo $X >> odd.txt
fi

((i++))
done
