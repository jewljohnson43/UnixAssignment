#!/bin/bash
#Author: Jewl B. Johnson II
#Descr: Prime Numbers

echo "Input a number"
read NUM

if [ $(($NUM % 2)) == 0 -o $(($NUM %3)) == 0 -o $((NUM%5)) == 0 ] 
then 

	echo "$NUM IS NOT A PRIME NUMBER"
else

	echo "NUM IS A PRIME NUMBER" 
fi
